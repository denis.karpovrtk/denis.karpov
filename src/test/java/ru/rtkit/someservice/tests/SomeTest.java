package ru.rtkit.someservice.tests;

import io.qameta.allure.Description;
import io.qameta.allure.Step;
import io.restassured.path.json.JsonPath;
import org.hamcrest.Matchers;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import ru.rtkit.someservice.apiHelper.Endpoints;
import ru.rtkit.someservice.apiHelper.Order;

import java.util.List;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("Проверка добавления питомца и заказа")
public class SomeTest extends BaseTest {

    @DisplayName("Добавление питомца")
    @Description("Проверка на добавление питомца")
    @ParameterizedTest(name = "создание питомца с именем {0}")
    @ValueSource(strings = {"Barsik", "BARSIK", "Барсик", "БАРСИК"})
    void hamcrestTest(String nameCats) {
        long petId = addNewPetToStore(nameCats);
        apiHelper.get(Endpoints.PET_ID, resp200, petId).then().body("name", Matchers.equalTo(nameCats));
    }

    @Step("Создание питомца {petNames}")
    long addNewPetToStore(String petNames) {

        JSONObject reqBody = new JSONObject()
                .put("id", 1)
                .put("category", new JSONObject().put("categoryName", "string")).put("categotyId", "1")
                .put("photoUrls", new JSONArray()
                        .put("string"))
                .put("tags", new JSONArray()
                        .put(new JSONObject().put("tagsName", "string").put("tagsId", "1")))
                .put("name", petNames)
                .put("status", "available");

        return apiHelper.post(Endpoints.NEW_PET, reqBody.toString(), resp200)
                .jsonPath()
                .getLong("id");
    }

    @DisplayName("Добавление заказа")
    @Description("Провекра на добавление заказа")
    @Test
    void assertionTest() {

        long orderId = addOrderToStore();
        JsonPath per = apiHelper.get(Endpoints.ORDER_ID, resp200, orderId).then().extract().jsonPath();

        assertEquals(1, per.getInt("petId"));
        assertInstanceOf(Boolean.class, per.get("complete"));
        assertNotNull(per.get("shipDate"));
        List<String> expectedStatuses = List.of("approved", "delivered", "placed");
        assertTrue(expectedStatuses.contains(per.get("status")));
    }

    @Step("Создание заказа")
    long addOrderToStore() {

        Order order= new Order();
        order.setId(1L);
        order.setPetId(1L);
        order.setQuantity(0L);
        order.setShipDate("2022-07-26T12:12:22.556Z");
        order.setStatus("placed");
        order.setComplete(false);

        JSONObject JSONorder = new JSONObject()
                .put("id", order.getId())
                .put("petId", order.getPetId())
                .put("quantity", order.getQuantity())
                .put("shipDate", order.getShipDate())
                .put("status", order.getStatus())
                .put("complete", order.getComplete());
        return apiHelper.post(Endpoints.ORDER, JSONorder, resp200).jsonPath().getLong("id");
    }

    @DisplayName("Поиск питомца по ID")
    @Description("Тест для проверки питомца по ID")
    @Test
    void test() {
        String cat = "Муся";

        long petId = addNewPetToStore(cat);
        apiHelper.get(Endpoints.PET_ID, resp200, petId);
    }
}

