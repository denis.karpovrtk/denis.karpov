package ru.rtkit.someservice.tests;

import io.restassured.specification.ResponseSpecification;
import ru.rtkit.someservice.apiHelper.ApiHelper;

import static io.restassured.RestAssured.expect;


public class BaseTest {

    public static ResponseSpecification resp200 = expect().statusCode(200);

    public static ApiHelper apiHelper;
    ;

    static {

        apiHelper = new ApiHelper();

    }
}
