package ru.rtkit.someservice.apiHelper;

public class Order {
    private Long id;
    private Long petId;
    private Long quantity;
    private String shipDate;
    private String status;
    private boolean complete;

    public Long getId(){
        return id;
    }

    public Long getPetId(){
        return petId;
    }

    public Long getQuantity(){
        return quantity;
    }

    public String getShipDate(){
        return shipDate;
    }

    public String getStatus(){
        return status;
    }

    public Boolean getComplete(){
        return complete;
    }

    public void setId(Long id){
        this.id = id;
    }

    public void setPetId(Long petId){
        this.petId = petId;
    }

    public void setQuantity(Long quantity){
        this.quantity = quantity;
    }

    public void setShipDate(String shipDate){
        this.shipDate = shipDate;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public void setComplete(Boolean complete){
        this.complete = complete;
    }

}
