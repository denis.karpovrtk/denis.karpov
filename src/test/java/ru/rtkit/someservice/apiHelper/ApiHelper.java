package ru.rtkit.someservice.apiHelper;

import io.qameta.allure.Step;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;


import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import io.restassured.specification.ResponseSpecification;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static io.restassured.RestAssured.given;


public class ApiHelper {

    public static Properties props;




    public ApiHelper() {
        InputStream is = ClassLoader.getSystemResourceAsStream( "service.properties");
        props = new Properties();
        try {
            props.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
        RestAssured.baseURI = props.getProperty("BASE_URL");
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.filters(new AllureRestAssured());

    }

    @Step("get {endpoint}")
    public Response get(String endpoint, ResponseSpecification resp) {
        return given()
                .contentType(ContentType.JSON)
                .expect()
                .spec(resp)
                .when()
                .get(endpoint);
    }

    @Step("get {endpoint}")
    public Response get(String endpoint, ResponseSpecification resp, Object... pathParams) {
        return given()
                .contentType(ContentType.JSON)
                .expect()
                .spec(resp)
                .when()
                .get(endpoint, pathParams);
    }

    @Step("delete {endpoint}")
    public Response delete(String endpoint, ResponseSpecification resp, Object... pathParams) {
        return given()
                .contentType(ContentType.JSON)
                .expect()
                .spec(resp)
                .when()
                .delete(endpoint, pathParams);
    }

    @Step("put {endpoint}")
    public Response put(String endpoint, Object body, ResponseSpecification resp, Object... pathParams) {
        return given()
                .contentType(ContentType.JSON)
                .body(body)
                .expect()
                .spec(resp)
                .when()
                .put(endpoint, pathParams);
    }

    @Step("post {endpoint}")
    public Response post(String endpoint, Object body, ResponseSpecification resp) {

        return given()
                .contentType(ContentType.JSON)
                .body(body.toString())
                .expect()
                .spec(resp)
                .when()
                .post(endpoint);
    }
}


