package ru.rtkit.someservice.apiHelper;

public class Endpoints {

    // добавление питомца
    public static final String NEW_PET = "/pet";
    public static final String PET_ID = "/pet/{petId}";

    // добавление в магазин
    public static final String ORDER = "/store/order";
    public static final String ORDER_ID = "/store/order/{orderId}";


}
