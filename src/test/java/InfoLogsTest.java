import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InfoLogsTest {

    public static Logger LOGGER = LoggerFactory.getLogger(InfoLogsTest.class);


    public void LogExample() {
        LOGGER.info("Здесь будет выведена информация");
    }


    public void LogError() {
        LOGGER.error("Сообщение об ошибке");
    }
}


