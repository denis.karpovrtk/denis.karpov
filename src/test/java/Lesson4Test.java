import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import org.hamcrest.Matchers;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;


import java.util.List;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.*;

public class Lesson4Test {

    int orderId = 1;
    int petId = 5;
    String name = "Barsik";

    @ParameterizedTest
    @ValueSource(strings = {"Barsik", "BARSIK", "Барсик", "БАРСИК"})
    void hamcrestTest(String nameCat) {
        postPet(nameCat);
        getPet(nameCat);
    }

   @Test
    void assertionTest() {
        orderPet();
        getOrderPet();
    }

    @Test
    void test() {
        postPet(name);
        getPet(name);
    }
    void getPet(String name) {
        given()
                .when()
                .pathParam("petId", petId)
                .get("https://petstore.swagger.io/v2/pet/{petId}")
                .then()
                .statusCode(200)
                .body("name", Matchers.equalTo(name));
    }

    void postPet(String nameCat) {
        JSONObject bodyObject = new JSONObject()
                .put("id", 0)
                .put("name", "string");

        JSONObject body = new JSONObject()
                .put("id", petId)
                .put("category", bodyObject)
                .put("name", nameCat)
                .put("photoUrls", new JSONArray()
                        .put("string"))
                .put("tags", new JSONArray()
                        .put(bodyObject))
                .put("status", "available");

        JsonPath per = given()
                .when()
                .contentType(ContentType.JSON)
                .body(body.toString())
                .post("https://petstore.swagger.io/v2/pet")
                .then()
                .statusCode(200)
                .contentType(ContentType.JSON)
                .extract().jsonPath();

    }
    void orderPet() {

        JSONObject order = new JSONObject()
                .put("id", orderId)
                .put("petId", petId)
                .put("quantity", 0)
                .put("shipDate", "2022-07-26T12:12:22.556Z")
                .put("status", "placed")
                .put("complete", false);

        given()
                .when()
                .contentType(ContentType.JSON)
                .body(order.toString())
                .post("https://petstore.swagger.io/v2/store/order")
                .then()
                .statusCode(200);
    }
    void getOrderPet() {
        String responseOrder = given()
                .when()
                .contentType(ContentType.JSON)
                .pathParam("id", orderId)
                .get("https://petstore.swagger.io/v2/store/order/{id}")
                .then()
                .statusCode(200).extract().response().body().asString();

        JSONObject response = new JSONObject(responseOrder);
        assertEquals(petId, response.get("petId"));
        assertInstanceOf(Boolean.class, response.get("complete"));
        assertNotNull(response.get("shipDate"));
        List<String> expectedStatuses = List.of("approved", "delivered", "placed");
        assertTrue(expectedStatuses.contains(response.get("status")));
    }
}
