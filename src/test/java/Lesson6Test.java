import org.junit.jupiter.api.Test;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Lesson6Test {
    String pathToDB =
            getClass().getClassLoader().getResource("sqlite/chinook.db").getPath();
    String dbUrl = "jdbc:sqlite:" + pathToDB;

    @Test
    void getType() {
        String query = "SELECT * FROM Media_types LIMIT 1";
        List<String> mediaType = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(dbUrl);
             PreparedStatement ps = conn.prepareStatement(query);
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                mediaType.add(rs.getString("name"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        System.out.println(mediaType);
    }

    @Test
    void getTracks() {
        String query = "SELECT * FROM Genres JOIN Tracks ON Genres.GenreId=Tracks.GenreId WHERE Genres.Name = 'Pop'";
        try (Connection conn = DriverManager.getConnection(dbUrl);
             PreparedStatement ps = conn.prepareStatement(query);
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                String genreName = rs.getString(2);
                int trackId = rs.getInt(3);
                String trackName = rs.getString(4);
                int albumId = rs.getInt(5);
                int mediaTypeId = rs.getInt(6);
                int genreId = rs.getInt(7);
                String composer = rs.getString(8);
                int multiseconds = rs.getInt(9);
                int bytes = rs.getInt(10);
                int unitPrice = rs.getInt(11);

                System.out.println("Tracks: " + genreName + " " + trackId + " " + trackName + " " + albumId + " "
                        + mediaTypeId + " " + genreId + " " + composer + " " + multiseconds + " " + bytes + " " + unitPrice);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }
}