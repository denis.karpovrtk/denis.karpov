import io.restassured.http.ContentType;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

public class Lesson2Test {


    void Pet(){
        postCat();
        putCat();
        getCat();
        deleteCat();
        catNotFound();
    }

    void postCat(){

        JSONObject bodyCat = new JSONObject()
                .put("id",5)
                .put("name", "Барсик")
                .put("status", "available");

        given()
                .when()
                .contentType(ContentType.JSON)
                .log().all()
                .body(bodyCat.toString())
                .post("https://petstore.swagger.io/v2/pet")
                .then()
                .statusCode(200);
    }


    void putCat(){

        String catsName = "Муся";
        JSONObject newCat = new JSONObject()
                .put("id",5)
                .put("name", catsName)
                .put("status", "available");

        given()
                .when()
                .contentType(ContentType.JSON)
                .body(newCat.toString())
                .put("https://petstore.swagger.io/v2/pet")
                .then()
                .log().all()
                .statusCode(200);
    }


    void getCat(){
        given()
                .when()
                .pathParam("petId",5)
                .get("https://petstore.swagger.io/v2/pet/{petId}")
                .then()
                .log().all()
                .statusCode(200);
    }


    void deleteCat(){
        given()
                .when()
                .pathParam("petId", 5)
                .delete("https://petstore.swagger.io/v2/pet/{petId}")
                .then()
                .log().all()
                .statusCode(200);
    }

    void catNotFound(){
        given()
                .when()
                .pathParam("petId",5)
                .get("https://petstore.swagger.io/v2/pet/{petId}")
                .then()
                .log().all()
                .statusCode(404);
    }
}
